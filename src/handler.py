# Skill calls will begin at the evaluate() method, with the skill payload passed 
# as 'payload' and the skill context passed as 'context'.
# For more details, see the AIOS User Manual.
import requests

SPACE_KEY = "IT-SERVICES Knowledge Base"

def evaluate(payload: dict, context: dict) -> dict:
    #id von https://wiki.wu.ac.at/display/KBITS/VPN+Verbindung+herstellen
    return {"response": get_from_confluence(31949306)}


def get_from_confluence(id):
    r = requests.get(f"https://wiki.wu.ac.at/rest/masterdetail/1.0/detailssummary/lines?cql=id={id}&spaceKey={SPACE_KEY}")
    # result = r.json()
    # resp_ger = result["detailLines"][0]["details"]
    # resp_eng = result["detailLines"][1]["details"]
    return r.text


